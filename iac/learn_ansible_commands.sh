#!/usr/bin/env bash
##################
#owner: droritzz
#purpose: learn ansible commands
#date: Saint Patrick's Day 2021
#version v1.0.1
##################

#check the remote machine
ansible web_servers -u vagrant --ask-pass -m ping

#module setup - all info about the remote machine
ansible web_servers -u vagrant -m setup 

# module setup with filter - example to filter according to filter argument
ansible web_servers -u vagrant -m setup -a "filter=ansible_python_version"  

#module copy - created some file - system update.
ansible web_servers -m copy -a "src=/full/path/to/source/script dest=/home/vagrant"

#module file - change permissions
ansible web_servers -b -m file -a "dest=/home/vagrant/system_update.sh mode=755 owner=root group=root"


